module.exports=function counterFactory ()
{
    let counter=0;
    return {
        increment:()=>counter+=1,decrement:()=>counter-=1
    }
}