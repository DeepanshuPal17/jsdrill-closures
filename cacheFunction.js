module.exports=function cacheFunction(cb)
{
    let cashed={};
    return function (...agrs){
        if(cashed[String(agrs)]==undefined)
        {
            cashed[String(agrs)]=cb(agrs);
            return cashed[String(agrs)];
        }
        else{
            return cashed[String(agrs)];
        }
    }
}