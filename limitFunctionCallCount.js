module.exports=function limitFunctionCallCount(cb,limit)
{
    return function (...agrs){
        if(limit>0)
        {
            limit-=1;
            let value=cb(agrs);
            if(value === undefined)
            {
                value=null;
            }
            return value;
        }
    }
}