const limitFunctionCallCount=require('../limitFunctionCallCount');

function cb()
{
    return "calling from cb function";
}

const limitFunction = limitFunctionCallCount(cb,2);
console.log(limitFunction());
console.log(limitFunction());
console.log(limitFunction());